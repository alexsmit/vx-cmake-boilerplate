# Disclaimer

This project is provided for educational and demonstration purpose only on the "AS-IS" basis. Sample code could be incomplete or outdated as well as provided readme files and comments.
Setup could not be considered as "production ready", may require additional work to be completed after 
a project is cloned locally.

# Project

(Original URL: https://gitlab.com/alexsmit/vx-cmake-boilerplate)

Simple boilerplate cmake project.
If you need to just create a simple C++ project in a minute.

Supported platforms: Linux only.

NOTE: if you need more advanced boilerplate that includes gtest, coverage, cppunit and more, check this out:
https://gitlab.com/alexsmit/vx-cmake-template

# Pre-requisites

c++ compiler
cmake
boost

# IDE
This project has everything ready to use VS Code to compile and run this project.

# Installation

After cloning the project rename a parent folder and delete .git derectory.
Run script:
./rename_project.sh

After this step all local names will be set to match a directory name you put this
project in.

# Running

To build a project you can use prepare_build.sh script.

- Clean
```
./prepare_build.sh -c
```

- Build
```
./prepare_build.sh run
```

- Install (expert funcrionality)
```
cd build
sudo make install
```

Sample program will be installed as /opt/sample/{project name}
Check the value of the parameter PROJECT_INSTALL_DIR in CMakeLists.txt

NOTE: if using a shared library, after the installation please run this command:
```
sudo ldconfig -v
```

# Using static/shared library

To disable building libraries comment this line:
```
set(USING_LIB 1) 
```
in CMakeLists.txt

To link using shared library comment this line:
```
set(LINK_TYPE static)
```
in CMakeLists.txt

# Todo

Just add some source files to ./src folder, build, debug and enjoy.
