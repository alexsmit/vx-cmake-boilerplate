#ifndef _STDAFX_H
#define _STDAFX_H

#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <list>
#include <map>
#include <memory>
#include <vector>

// this is necessary for some old boost libraries
#define BOOST_SPIRIT_THREADSAFE

#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>
#include <boost/optional.hpp>
#include <boost/variant.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#endif
