project(vx-cmake-boilerplate)
cmake_minimum_required(VERSION 3.10)

# recommended minimum standard
set(CMAKE_CXX_STANDARD 14)

# include_directories(... some include directory ...)
find_package(Boost 1.65.1 COMPONENTS system REQUIRED)

include_directories(../)

file(GLOB SOURCES
    ../src/*.h
    ../src/*.cpp
)

if(LINK_TYPE STREQUAL static)
    add_library(vx-cmake-boilerplate_a ${SOURCES})
else()
    add_library(vx-cmake-boilerplate_lib SHARED ${SOURCES})
endif()
